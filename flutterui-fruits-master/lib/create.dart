import 'package:flutter/material.dart';
import 'package:fruitsui/login.dart';
import 'api.dart';
import 'user.dart';
class CreateAccount extends StatefulWidget{
  _CreateAccountState createState() => _CreateAccountState();
}
class _CreateAccountState extends State<CreateAccount>
{
   final _formKey = GlobalKey<FormState>();
   final _username = TextEditingController();
   final _email = TextEditingController();
   final _pass = TextEditingController();
   final _conpass = TextEditingController();
      Widget build(BuildContext context) {
     return MaterialApp(
    theme: ThemeData(
      scaffoldBackgroundColor: Colors.white,
    ),
    home: Scaffold(
      extendBodyBehindAppBar: true,
      body: Container(
        
        child:Form(
          key: _formKey,
        child: SingleChildScrollView(child: 
        Column(
          children:[
            SizedBox(height:100.0),
            Row(children:[
Spacer(),
                             IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.green[800],
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                Spacer(),
                     Align(
              alignment: Alignment.center,
              child:  Text(
                        'Sign Up',
                        style: TextStyle(
                          color: Colors.green[800],
                         
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
            ),
            Spacer(),
            Spacer(),
            ]),
       
             SizedBox(height:10.0),
             Align(
               alignment: Alignment.center,
               child: Text('Please enter the following details',style: TextStyle(
                          color: Colors.grey,
                          
                          fontSize: 18.0,
                          letterSpacing: 0.2 ,
                          fontWeight: FontWeight.w500,
                         
                        ),),
             ),
                  SizedBox(height:20.0),   
            NameInput(),
             SizedBox(height:10.0),
             EmailInput(),
                SizedBox(height:10.0),
            _buildPasswordTF(),
             SizedBox(height:10.0),
             _conpassfunc(),
                SizedBox(height:10.0),
              RaisedButton(
                onPressed:  _validateInputs,
                color: Colors.green[800],
                textColor: Colors.white,
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: Text('Create Account'),
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
              ),
              SizedBox(height:10.0),
                   
          ]
        ),),)
    
      ),
    ),
  );
 
  }
 
      Widget NameInput() {
   return TextFormField(
	autocorrect: true,
  controller: _username,

	decoration: InputDecoration(
    hintText: 'Username', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),);
  }
      Widget EmailInput() {
   return TextFormField(
	autocorrect: true,
  controller: _email,

	decoration: InputDecoration(
    hintText: 'Email', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),);
  }
    Widget _buildPasswordTF() {
  return TextFormField(
	autocorrect: true,
  controller: _pass,
   obscureText: true,
	decoration: InputDecoration(
    hintText: 'Password', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),);
      
  }
    Widget _conpassfunc() {
  return TextFormField(
	autocorrect: true,
  controller: _conpass,
   obscureText: true,
	decoration: InputDecoration(
    hintText: 'Confirm Password', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),);
      
  }
   void _validateInputs() async
  {
     _formKey.currentState.save();
     LoginClass user= new LoginClass.novalue();
     user.username = _username.text;
     user.email = _email.text;
     user.password = _pass.text;
    API.Api.create(user).then((value) {
     if(value.body.isNotEmpty)
     {
        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => LoginPage(login_page: 0)));
     }
    });
  }
}