import 'package:flutter/material.dart';
import 'package:fruitsui/fruits.dart';
import 'package:fruitsui/main.dart';
import 'package:fruitsui/vegetable.dart';
import 'dart:convert';
import 'dart:io';
import 'package:fruitsui/api.dart';
import 'home1.dart';
import 'home.dart';
import 'create1.dart';
import 'login.dart';
import 'cart.dart';
import './myorder.dart';
import 'package:badges/badges.dart';
import './highlyrecommended.dart';

class JuicesPage extends StatefulWidget {
  @override
JuicesPageState createState() => JuicesPageState();
}

class JuicesPageState extends State<JuicesPage>

    with SingleTickerProviderStateMixin {
      final globalKey = GlobalKey<ScaffoldState>();
  TabController tabController;
    bool cart= false;
     var count;
  String dropdownValue = 'One';
  List<String> fruitid =['f1','f2'];
  void initState() {
 
 if(id!=0)
 {
    getcartcount();
 }

    // This is the initial data // Set it in initState because you are using a stateful widget
super.initState();
  }
    void getcartcount() async {
    API.Api.getcartcount(id.toString()).then((value){
      
      setState(() {
        count = json.decode(value.body);
        cart=true;
      });
    });
    
  }

   Widget _buildTile(Widget child, {Function() onTap}) {
    return Material(
      elevation: 14.0,
      borderRadius: BorderRadius.circular(12.0),
      shadowColor: Color(0x802196F3),
      child: InkWell
      (
        // Do onTap() if it isn't null, otherwise do print()
        onTap: onTap != null ? () => onTap() : () { print('Not set yet'); },
        child: child
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: globalKey,
      drawer: Drawer(child: ListView(
        padding: EdgeInsets.zero,
        children: [
           DrawerHeader(
        child: Text(''),
        decoration: BoxDecoration(
          color: Colors.white,
        ),
      ),
      ListTile(
              leading: Icon(Icons.home,color: Colors.green[800],),
        title: Text('Home'),
        
        onTap: () {
        
           Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => MyHomePage_l1()),
  ); 


          }
       
      ),
            ListTile(
              leading: Icon(Icons.star_border,color: Colors.green[800],),
        title: Text('Fruits'),
        
        onTap: () {
          Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => FruitsPage()),
  );
          // Update the state of the app.
          // ...
        },
      ),
       ListTile(
              leading: Icon(Icons.star_border,color: Colors.green[800],),
        title: Text('Vegetables'),
        
        onTap: () {
          Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => VegetablePage()),
  );
          // Update the state of the app.
          // ...
        },
      ),      ListTile(
              leading: Icon(Icons.star_border,color: Colors.green[800],),
        title: Text('Juices'),
        
        onTap: () {
          if (id !=0) {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => JuicesPage()));
        } else  {
         
           
            Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) =>LoginPage(login_page: 1)));
        }
          // ...
        },
      ),
      
      
        ListTile(
              leading: Icon(Icons.history,color: Colors.green[800],),
        title: Text('My Orders'),
        
        onTap: () {
        
           Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => OrderPage()),
  ); 


          }
       
      ), 
      ListTile(
              leading: Icon(Icons.logout,color: Colors.green[800],),
        title: Text('Logout'),
        
        onTap: () {
         logout(id.toString());

          }
       
      ),
      ],
      ),),
   
        body:
        ListView( 
      children: <Widget>[
        SizedBox(
          height: 10.0,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.menu, color: Colors.grey),
              onPressed: () {
                globalKey.currentState.openDrawer();
              },
            ),
            Spacer(),
            Container(
              height: 50.0,
              width: 200.0,
              decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.1),
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(25.0),
                      bottomLeft: Radius.circular(25.0),
                      topLeft: Radius.circular(25.0),
                      topRight: Radius.circular(25.0))),
              child: TextField(
                decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(top: 15.0),
                    prefixIcon: Icon(Icons.search, color: Colors.grey)),
              ),
            ),
            Spacer(),
cart?    Badge(
      badgeContent: Text("$count",style: TextStyle(color:Colors.white),),
      child:  IconButton(
        icon: Icon(Icons.shopping_cart,color: Colors.green[800],),
      
        onPressed: (){
          
        if (name !=null) {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => CartPage()));
        } else  {
         
           
            Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) =>LoginPage(login_page: 2)));
        }
        },
      ),
      badgeColor: Colors.green[800],
      
    ):Spacer(),
    Spacer(),
          ],
        ),
        SizedBox(height: 10.0),
        Padding(
          padding: EdgeInsets.only(left: 10.0),
          child: Text('Juices',
              style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontWeight: FontWeight.bold,
                  fontSize: 25.0)),
        ),
        SizedBox(height: 50.0,),
         Row(
              
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Spacer(),
                 _buildTile(
            Padding(
              padding: const EdgeInsets.all(35.0),
              child: Column
              (
                mainAxisAlignment: MainAxisAlignment.start,
                
                children: <Widget>
                [
                  Material
                  (
                    color: Colors.white,
                    shape: CircleBorder(),
                    child: Padding
                    (
                      padding: const EdgeInsets.only(right:15.0),

                      child: new Image.asset(
          'assets/juice_belnder.png'),
                    )
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 16.0)),
                   Text('Create', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 20.0)),
                  Text('Your Own Magic', style: TextStyle(color: Colors.black45)),
                  
                ]
              ),
              
            ),
            onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => Selection(),
          ),
          ),
          ),           
          Spacer(),
          _buildTile(
            Padding(
              padding: const EdgeInsets.all(35.0),
              child: Column
              (
                mainAxisAlignment: MainAxisAlignment.start,
                
                children: <Widget>
                [
                  Material
                  (
                    color: Colors.white,
                    shape: CircleBorder(),
                    child: Padding
                    (
                      padding: const EdgeInsets.only(right:15.0),

                      child: new Image.asset(
          'assets/juice_heart.png'),
                    )
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 16.0)),
                   Text('Buy', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 20.0)),
                  Text("Other's Magic", style: TextStyle(color: Colors.black45)),
                  
                ]
              ),
              
            ),
            onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => HighlyR(),
          ),
          ),
          ),           
          Spacer(),

              ],
            )
      ],
        ),
        
    );

  }

  logout(String uid)
  {
    API.Api.deleteallcart(uid).then((value) {
      if(value.statusCode == 200)
      {
        setState(() {
           id=0;
        name=null;
        });
       
           Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => MyHomePage(),
        ));
      }
    });
  }
  
 

}
    