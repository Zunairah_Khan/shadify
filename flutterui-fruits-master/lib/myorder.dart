import 'package:flutter/material.dart';
import 'package:fruitsui/api.dart';
import 'dart:convert';
import 'login.dart';
class OrderPage extends StatefulWidget {
  
  @override
  _OrderPageState createState() => _OrderPageState();
}

class _OrderPageState extends State<OrderPage> {
  @override
     List<dynamic> list;
     bool fruit = false;
       void initState() {
 getorders();
super.initState();
       }
        void getorders() async{
    API.Api..gethistory(id.toString()).then((value) {
      var data = json.decode(value.body);
      print(data);
      var res = data as List;
      setState(() {
        list = res;
        fruit = true;
      });
      
    });
  }
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green[800],
      appBar: AppBar(
      elevation: 0.1,
      backgroundColor: Colors.green[800],
      title: Text("Orders"),
      
    ),
      body: Container(
      child:fruit? ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: list.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: Container(
        decoration: BoxDecoration(color: Colors.white),
        child: ListTile(
        contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
        leading: Container(
          padding: EdgeInsets.only(right: 12.0),
          decoration: new BoxDecoration(
              border: new Border(
                  right: new BorderSide(width: 1.0, color: Colors.white24))),
          child: Icon(Icons.favorite, color: Colors.green),
        ),
        title: Text(
          list[index]['Orderno'],
          style: TextStyle(color: Colors.green[800], fontWeight: FontWeight.bold),
        ),
        // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

        subtitle: Column(
          children: <Widget>[
            Row(children: [
                   Icon(Icons.calendar_today_outlined, color: Colors.green[800]),
            Text(" ${list[index]['order_date']}", style: TextStyle(color: Colors.green[800]))
            ],)
         
          ],
        ),
        trailing:
            Text("Rs. ${list[index]['total']}",style: TextStyle(color: Colors.green[800]))
      )));
    
        },
      ):Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                ],
              ))

    ),
     
    );
  }
}