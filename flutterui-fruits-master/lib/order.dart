import 'package:flutter/material.dart';
import 'package:fruitsui/home1.dart';
import 'home.dart';
import 'api.dart';
import 'login.dart';
class Order extends StatefulWidget{
  Order_State createState() => new Order_State();
}
class Order_State extends State<Order>
{
  void initState() {

 logout(id.toString());

    // This is the initial data // Set it in initState because you are using a stateful widget
super.initState();
  }
  Widget build(BuildContext context) {
  return Scaffold(
    backgroundColor: Colors.white,
      appBar: AppBar(
        title: Row(children: <Widget>[
           Text(
                  "Fridge It - Let's Buy Fresh",
                  textAlign: TextAlign.center,
                ),
                Spacer(),
                
                IconButton(
                  icon: Icon(
                    Icons.logout,
                    color: Colors.white,
                  ),
                  onPressed: null,
                ),
          
        ]),
        backgroundColor: Colors.green[800],
        automaticallyImplyLeading: false,
        elevation: 5.0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
           
            Container(
                margin: const EdgeInsets.only(
                    top: 0.0, left: 5.0, right: 5.0, bottom: 50.0),
                padding: EdgeInsets.all(150),
                decoration: new BoxDecoration(
                    image: new DecorationImage(
                  image: new AssetImage("assets/delivery.jpg"),
                  fit: BoxFit.cover,
                ))),
                SizedBox(
                  height:10.0,
                ),
              Text("Order Confirmed",style: TextStyle(color: Colors.green[800],fontSize: 20.0),),
               SizedBox(
                  height:10.0,
                ),
              Text("Our Representator will call you soon",style: TextStyle(color: Colors.green[800],fontSize: 20.0),),
              RaisedButton(
                onPressed: (){
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => MyHomePage_l1()));
                },
                color: Colors.green[800],
                textColor: Colors.white,
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: Text(
                  'Redirect To Home',
                  style: TextStyle(fontSize: 15),
                ),
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
              ),

          ]

          )));
}
 logout(String uid)
  {
    API.Api.deleteallcart(uid);
  }
}