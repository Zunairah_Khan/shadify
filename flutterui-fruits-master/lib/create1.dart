

import 'package:flutter/material.dart';
import 'package:flutter_multiselect/flutter_multiselect.dart';
import 'package:fruitsui/juicemodel.dart';
import 'cart.dart';
import 'dart:convert';
import 'dart:io';
import 'package:fruitsui/api.dart';
import './login.dart';

class Selection extends StatefulWidget {
  @override
  Selection_State createState() => new Selection_State();
}
class Selection_State extends State<Selection>{
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  Selection_State();
   List<dynamic> list;
   bool juice = false;
   List<String> my=[];
   int total=0;
   int jtotal =0;
   int barprice;
   String baridf;
  final globalKey = GlobalKey<ScaffoldState>();
  void initState() {
 getjuice();
 
    // This is the initial data // Set it in initState because you are using a stateful widget
super.initState();
  }
getjuice() async
{
  API.Api.getjuice().then((value) {
    var data = json.decode(value.body);
    var res = data as List;
    setState(() {
        list = res;
        juice = true;
      });
  });
}
int juice_id;
List<dynamic> fruits = [];
List<dynamic> vegetables = [];
  bool _autoValidate = false;
String jname;
 final juice_name = TextEditingController();
List<dynamic> images=['assets/juice1.png','assets/juice2.png',];

   Widget build(BuildContext context) {
     return Scaffold(
       backgroundColor:Colors.white,
        key: globalKey,
          appBar: AppBar(
        title: Row(children: <Widget>[
          IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                Spacer(),
          
                Spacer(),
                
              
          
        ]),
        backgroundColor: Colors.green[800],
        automaticallyImplyLeading: false,
        elevation: 5.0,
      ),
         
        body:juice? SingleChildScrollView(
        child: Form(
          key: _formKey,
          
          autovalidate: true,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
             
              
                Container(
                margin: const EdgeInsets.only(
                    top: 0.0, left: 5.0, right: 5.0, bottom: 0.0),
                padding: EdgeInsets.all(130),
                decoration: new BoxDecoration(
                    image: new DecorationImage(
                  image: new AssetImage("assets/head.jpg"),
                  fit: BoxFit.cover,
                ))),
              Padding(
                padding: const EdgeInsets.all(0.0),
                child: new MultiSelect(
                selectedOptionsInfoTextColor: Colors.green[800],
                
                    autovalidate: true,
                    initialValue: ['IN', 'US'],
                    titleText: 'Select Ingredients',
                    maxLength: 5, // optional
                    validator: (dynamic value) {
                      if (value == null) {
                        return 'Please select one or more option(s)';
                      }
                      return null;
                    },
                    errorText: 'Please select one or more option(s)',
                    dataSource: list,
                    textField: 'Text',
                    
                    valueField: 'Value',
                    filterable: true,
                    required: true,
                    value: null,
                    change: (values)
                    {
                        fruits = values;
                       
                     
                    },
                    onSaved: (value) {
                      fruits = value;
                    },
                    
                    titleTextColor: Colors.green[800],
                    selectIcon: Icons.arrow_drop_down_circle,
                    selectIconColor: Colors.green[800],
                    saveButtonColor: Colors.green[800],
                    checkBoxColor: Colors.green[800],
                    cancelButtonColor:Colors.green[800] ,
                    buttonBarColor: Colors.white,
                    cancelButtonTextColor: Colors.white,
                    clearButtonTextColor: Colors.white,
                    clearButtonColor: Colors.green[800],
                    ),
              ),
              SizedBox(
                height: 20.0,
              ),
         TextFormField(
     controller: juice_name,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "Juice Name",
        labelStyle: TextStyle(color: Colors.black),
        hintText: "",
      ),
      textInputAction: TextInputAction.done,
      validator: (String args) {
        if (args == "")
          return 'Please fill this field';
         else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    ),
      SizedBox(
                height: 20.0,
              ),
    Container(
                    height: 200,
                    child: GridView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: images.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 1, crossAxisSpacing: 4.0, mainAxisSpacing: 4.0),
                      itemBuilder: (BuildContext context , int index){
                        return Container(
                          child: Card(
                            child: new InkResponse(
                              splashColor: Colors.green[800],
                              child: Image.asset(images[index]),
                              onTap: ()=> get_id(index+1),
                            ),),);},),),
                    Row(
                      children:[
                        Spacer(),
                         RaisedButton(
                child: Text('Get Price',style: TextStyle(color:Colors.white),),
                color: Colors.green[800],
                onPressed: () {
                 getprice();
                  
                   },),Spacer(),
                           RaisedButton(
                child: Text('Add to Cart',style: TextStyle(color:Colors.white),),
                color: Colors.green[800],
                onPressed: () {
                 _onFormSaved();
                  
                   },),Spacer(),
                   ]
                    )
                   ],),))
                  :Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                ],
              ))
        );
 
   }
   void get_id(int barid1)
   {
     String barid = 'b'+barid1.toString();
     API.Api.getbarprice(barid).then((value) {
     var data = json.decode(value.body);
      var res = data as int;
      setState(() {
        barprice = res;
        baridf = barid;
      });
     });
    
   }
   getprice() async
{
  int check=0;
  for(int i=0;i< fruits.length;i++)
  {
  API.Api.getfruitprice(fruits[i]).then((value){
  check = check + int.parse(value.body);
  print(check);
  if(i== (fruits.length -1))
  {
    jtotal = check;
    print(jtotal); 
  //  _onFormSaved();
  }
   
});
  }
}
    void _onFormSaved() {
     
     _autoValidate = true;
    final FormState form = _formKey.currentState;
    var sList = List<String>.from(fruits);
    print(sList);
  jname = juice_name.text;
  total = barprice + jtotal;
print(baridf);
  JuiceClass j = new JuiceClass.novalue();
  j.jname = juice_name.text;
  j.fid = sList;
  j.price= total;
j.bar = baridf;
 API.Api.insertjuice(jname, total, id.toString());
 checkdata(j);
    form.save();
    showAlertDialog(context);
  }
    showAlertDialog(BuildContext context) {  
  // Create button  
  Widget Shop = FlatButton(  
    child: Text("Continue Shopping"),  
    onPressed: () {  
      Navigator.of(context).pop();  
    },  
   
  );  
  Widget Checkout = FlatButton(  
    child: Text("Checkout"),  
   onPressed: () {  
       Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => CartPage()),
  );
    },  
  );  
  
  // Create AlertDialog  
  AlertDialog alert = AlertDialog(  
    title: Text("Wohoo",style: TextStyle(color:Colors.green[800]),),  
    content: Text("Item Added to Cart"),  
    actions: [  
      Shop,
      Checkout  
    ],  
  );  
  
  // show the dialog  
  showDialog(  
    context: context,  
    builder: (BuildContext context) {  
      return alert;  
    },  
  );  
} 
checkdata(JuiceClass jm) async
  {
   
    API.Api.insertjuicehistory(jm).then((value){
     
    });
  }
}
/// This is the main application widget.
