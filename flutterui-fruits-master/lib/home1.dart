import 'package:flutter/material.dart';
import 'package:fruitsui/juice.dart';
import 'package:fruitsui/cart.dart';
import 'package:fruitsui/vegetable.dart';
import './home.dart';
import 'fruits.dart';
import 'login.dart';
import 'package:fruitsui/api.dart';
import 'dart:convert';
import 'package:badges/badges.dart';

class MyHomePage_l1 extends StatefulWidget {

  final String title;
  MyHomePage_l1({Key key, this.title,})
      : super(key: key);

  @override
  _MyHomePageState_l createState() =>
      new _MyHomePageState_l();
}
class _MyHomePageState_l extends State<MyHomePage_l1> {
  
  _MyHomePageState_l();
 bool cart= false;
     var count;
 void initState() {

 if(id!=0)
 {
    getcartcount();
 }

    // This is the initial data // Set it in initState because you are using a stateful widget
super.initState();
  }
    void getcartcount() async {
    API.Api.getcartcount(id.toString()).then((value){
      
      setState(() {
        count = json.decode(value.body);
        cart=true;
      });
    });
    
  }
 Widget _buildTile(Widget child, {Function() onTap}) {
    return Material(
      elevation: 14.0,
      borderRadius: BorderRadius.circular(12.0),
      shadowColor: Color(0x802196F3),
      child: InkWell
      (
        // Do onTap() if it isn't null, otherwise do print()
        onTap: onTap != null ? () => onTap() : () { print('Not set yet'); },
        child: child
      )
    );
  }
   

  @override
  Widget build(BuildContext context) {
    if(name != null)
    {
        print(name);
    }
  
   //Future.delayed(Duration.zero, () => showAlert(context));
  return Scaffold(
      appBar: AppBar(
        title: Row(children: <Widget>[
           Text(
                  "Fridge It - Let's Buy Fresh",
                  textAlign: TextAlign.center,
                ),
                Spacer(),
                  cart?    Badge(
      badgeContent: Text("$count",style: TextStyle(color:Colors.white),),
      child:  IconButton(
        icon: Icon(Icons.shopping_cart,color: Colors.white,),
      
        onPressed: (){
          
        if (name !=null) {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => CartPage()));
        } else  {
         
           
            Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) =>LoginPage(login_page: 2)));
        }
        },
      ),
      badgeColor: Colors.green[800],
      
    ):Spacer(),
                
                IconButton(
                  icon: Icon(
                    Icons.logout,
                    color: Colors.white,
                  ),
                  onPressed: (){
                    logout(id.toString());
                  },
                ),
           

        ]),
        backgroundColor: Colors.green[800],
        automaticallyImplyLeading: false,
        elevation: 5.0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
           
            Container(
                margin: const EdgeInsets.only(
                    top: 0.0, left: 5.0, right: 5.0, bottom: 50.0),
                padding: EdgeInsets.all(150),
                decoration: new BoxDecoration(
                    image: new DecorationImage(
                  image: new AssetImage("assets/bg_1.jpg"),
                  fit: BoxFit.cover,
                ))),
            Row(
              
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Spacer(),
                 _buildTile(
            Padding(
              padding: const EdgeInsets.all(38.0),
              child: Column
              (
                mainAxisAlignment: MainAxisAlignment.start,
                
                children: <Widget>
                [
                  Material
                  (
                    color: Colors.white,
                    shape: CircleBorder(),
                    child: Padding
                    (
                      padding: const EdgeInsets.only(right:20.0),

                      child: new Image.asset(
          'assets/watermelon_home.png'),
                    )
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 16.0)),
                  Text('Fruits     ', style: TextStyle(color: Colors.green[800], fontWeight: FontWeight.w700, fontSize: 25.0)),
                  
                ]
              ),
              
            ),
            onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => FruitsPage(),
          ),
          ),
          ),           
          Spacer(),
          _buildTile(
            Padding(
              padding: const EdgeInsets.all(38.0),
              child: Column
              (
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>
                [
                  Material
                  (
                    color: Colors.white,
                    shape: CircleBorder(),
                    child: Padding
                    (
                      padding: const EdgeInsets.all(5.0),
                       child: new Image.asset(
          'assets/carrot_home.png'),
                    )
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 16.0)),
                  Text('Vegetables', style: TextStyle(color: Colors.green[800], fontWeight: FontWeight.w700, fontSize: 20.0)),
                  
                ]
              ),
              
            ),
            onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => VegetablePage(),
          ),
          ),
          ),
          Spacer(),
         
              ],
            ),
        SizedBox(height:10.0),
                        Row(
              
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Spacer(),
                 _buildTile(
            Padding(
              padding: const EdgeInsets.all(38.0),
              child: Column
              (
                mainAxisAlignment: MainAxisAlignment.start,
                
                children: <Widget>
                [
                  Material
                  (
                    color: Colors.white,
                    shape: CircleBorder(),
                    child: Padding
                    (
                      padding: const EdgeInsets.only(right:20.0),

                child: new Image.asset(
          'assets/orange-juice_home.png'),
                    )
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 16.0)),
                  Text('Juices    ', style: TextStyle(color: Colors.green[800], fontWeight: FontWeight.w700, fontSize: 25.0)),
                  
                ]
              ),
              
            ),
onTap: () {
        if (name !=null) {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => JuicesPage()));
        } else  {
         
           
            Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) =>LoginPage(login_page: 1)));
        }
      },     
            ),    
          Spacer(),
     
         
              ],
            ),
          ],
        ),
      ),
    );
  }
  logout(String uid)
  {
    API.Api.deleteallcart(uid).then((value) {
      if(value.statusCode == 200)
      {
        setState(() {
           id=0;
        name=null;
        });
       
           Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => MyHomePage(),
        ));
      }
    });
  }
}
