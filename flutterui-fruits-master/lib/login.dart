import 'package:flutter/material.dart';
import 'package:fruitsui/api.dart';
import 'package:fruitsui/fruits.dart';
import 'package:fruitsui/vegetable.dart';
import 'package:validators/validators.dart';
import 'package:fruitsui/cart.dart';
import 'package:flutter/gestures.dart';
import 'package:fruitsui/home1.dart';
import './create.dart';

import 'package:fruitsui/user.dart';
import 'juice.dart';
import 'home1.dart';
import 'dart:convert';

String name;
int id =0;
class LoginPage extends StatefulWidget {
  final int login_page;
  final String itemid;
LoginPage({Key key, @required this.login_page,this.itemid})
      : super(key: key);
  @override
  _LoginPageState createState() => _LoginPageState(login_page: this.login_page,itemid: this.itemid);
}
class _LoginPageState extends State<LoginPage> {
   final _formKey = GlobalKey<FormState>();
    int login_page;
  String itemid;
  final _username = TextEditingController();
  final _password = TextEditingController();
  _LoginPageState({this.login_page,this.itemid});

 
   Widget build(BuildContext context) {
        print(itemid);
     return MaterialApp(
    theme: ThemeData(
      scaffoldBackgroundColor: Colors.white,
    ),
    home: Scaffold(
      extendBodyBehindAppBar: true,
      body: Container(
        
        child:Form(
          key: _formKey,
        child: SingleChildScrollView(child: 
        Column(
          children:[
            SizedBox(height:100.0),
            
            Align(
              alignment: Alignment.center,
              child:  Text(
                        'Login',
                        style: TextStyle(
                          color: Colors.green[800],
                         
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
            ),
             SizedBox(height:10.0),
             Align(
               alignment: Alignment.center,
               child: Text('Please login to continue',style: TextStyle(
                          color: Colors.grey,
                          
                          fontSize: 18.0,
                          letterSpacing: 0.2 ,
                          fontWeight: FontWeight.w500,
                         
                        ),),
             ),
                  SizedBox(height:20.0),   
            NameInput(),
             SizedBox(height:10.0),
            _buildPasswordTF(),
             SizedBox(height:10.0),
              RaisedButton(
                onPressed:  _validateInputs,
                color: Colors.green[800],
                textColor: Colors.white,
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: Text('Sign In'),
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
              ),
         
              SizedBox(height:10.0),
                     Row(
                            children:[
                              Spacer(),
                              RichText(text: TextSpan(text: "Don't have an Account?",style: TextStyle(fontSize:18.0, color: Colors.grey),
                              children: <TextSpan>[
                                TextSpan(
                                  text:' Sign Up',
                                   recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      Navigator.of(context).push(
              MaterialPageRoute(
                builder: (
                  context,
                ) {
                  // _showLoadingDialog();
                  return CreateAccount(
                    
                  );
                },
              ),
            );
                    },style: TextStyle(fontSize:18.0,color:Colors.green[800]
                                )
                              )])),
                          Spacer(),
                            ]
                          ),
          ]
        ),),)
    
      ),
    ),
  );
 
  }

  void _validateInputs() async
  {
     _formKey.currentState.save();
     LoginClass user= new LoginClass.novalue();
     user.username = _username.text;
  
     user.password = _password.text;
    API.Api.login(user).then((value) {
      var data = json.decode(value.body);
      if(value.body.isEmpty)
      {
        name=null;
         showAlertDialog(context);  
      }
      else
      {
        setState(() {
          
        id = data['id'];
        name = data['Username'];
        });
        if(login_page == 1)
       {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => JuicesPage(),
        ));
       }
       else  if(login_page ==2)
       {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => CartPage(),
        ));
       }
       else  if(login_page ==3)
       {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => FruitsPage(),
        ));
       }
       else  if(login_page ==3)
       {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => VegetablePage(),
        ));
       }
       
       else if(itemid != null)
       {
          additem(itemid);
       }
       else
       {
         
         Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => MyHomePage_l1(),
        ));
      }
      }
    });
    }
    Widget NameInput() {
   return TextFormField(
	autocorrect: true,
  controller: _username,

	decoration: InputDecoration(
    hintText: 'Username', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),);
  }
  Widget _buildPasswordTF() {
  return TextFormField(
	autocorrect: true,
  controller: _password,
   obscureText: true,
	decoration: InputDecoration(
    hintText: 'Password', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),);
      
  }
  additem(String iid) async
  {
     API.Api.cart(iid, id.toString()).then((value) {
        if(value.statusCode == 200)
        {
          showAlertDialog1(context);  
        }
        });
  }
showAlertDialog1(BuildContext context) {  
  // Create button  
  Widget Shop = FlatButton(  
    child: Text("Continue Shopping"),  
    onPressed: () {  
        Navigator.push(
    context,
    MaterialPageRoute(builder: (context) =>MyHomePage_l1 ()),
  );
    },  
   
  );  
  Widget Checkout = FlatButton(  
    child: Text("Checkout"),  
   onPressed: () {  
       Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => CartPage()),
  );
    },  
  );  
  
  // Create AlertDialog  
  AlertDialog alert = AlertDialog(  
    title: Text("Wohoo",style: TextStyle(color:Colors.green[800]),),  
    content: Text("Item Added to Cart"),  
    actions: [  
      Shop,
      Checkout  
    ],  
  );  
  
  // show the dialog  
  showDialog(  
    context: context,  
    builder: (BuildContext context) {  
      return alert;  
    },  
  );  
}  
  showAlertDialog(BuildContext context) {  
  // Create button  
  Widget okButton = FlatButton(  
    child: Text("OK"),  
    onPressed: () {  
      Navigator.of(context).pop();  
    },  
  );  
  
  // Create AlertDialog  
  AlertDialog alert = AlertDialog(  
    title: Text("Error",style: TextStyle(color:Colors.red),),  
    content: Text("Invalid Login"),  
    actions: [  
      okButton,  
    ],  
  );  
  
  // show the dialog  
  showDialog(  
    context: context,  
    builder: (BuildContext context) {  
      return alert;  
    },  
  );  
}  
}