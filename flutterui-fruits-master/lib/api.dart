import 'dart:convert';

import 'package:fruitsui/juicemodel.dart';
import 'package:fruitsui/orderclass.dart';
import 'package:fruitsui/user.dart';
import 'package:fruitsui/vegetable.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:core';
class API{
   static API Api;
  Future<http.Response> login(LoginClass user) async {
    return http.post(
      Uri.parse('http://192.168.0.163:4000/api/Login/login'),
      headers:<String,String>{
         'Content-Type': 'application/json; charset=UTF-8',
      },
      body: json.encode(user),
    );
  }
  Future<http.Response> create(LoginClass user) async {
    return http.post(
      Uri.parse('http://192.168.0.163:4000/api/Login/adduser'),
      headers:<String,String>{
         'Content-Type': 'application/json; charset=UTF-8',
      },
      body: json.encode(user),
    );
  }
  Future<http.Response> fruit() async {
    return http.get(
      Uri.parse('http://192.168.0.163:4000/api/Login/Getfruits'),
      headers:<String,String>{
         'Content-Type': 'application/json; charset=UTF-8',
      },
      
    );
  }
  Future<http.Response> vegetable() async {
    return http.get(
      Uri.parse('http://192.168.0.163:4000/api/Login/GetVegetable'),
      headers:<String,String>{
         'Content-Type': 'application/json; charset=UTF-8',
      },
      
    );
  }
   Future<http.Response> cart(String fid,String userid) async {
    return http.get(
      Uri.parse('http://192.168.0.163:4000/api/Login/AddtoCart?id1=$fid&userid=$userid'),
      headers:<String,String>{
         'Content-Type': 'application/json; charset=UTF-8',
      },
      
    );
  }
     Future<http.Response> getcart(String userid) async {
    return http.get(
      Uri.parse('http://192.168.0.163:4000/api/Login/Getcart?userid=$userid'),
      headers:<String,String>{
         'Content-Type': 'application/json; charset=UTF-8',
      },
      
    );
  }
  Future<http.Response> getcartcount(String userid) async {
    return http.get(
      Uri.parse('http://192.168.0.163:4000/api/Login/GetCartCount?userid=$userid'),
      headers:<String,String>{
         'Content-Type': 'application/json; charset=UTF-8',
      },
      
    );
  }
    Future<http.Response> deletefromcart(String item_id,String userid) async {
    return http.get(
      Uri.parse('http://192.168.0.163:4000/api/Login/DeleteCart?item_name=$item_id&userid=$userid'),
      headers:<String,String>{
         'Content-Type': 'application/json; charset=UTF-8',
      },
      
    );
  }
  Future<http.Response> addincart(String item_id,String userid) async {
    return http.get(
      Uri.parse('http://192.168.0.163:4000/api/Login/AddCart?item_name=$item_id&userid=$userid'),
      headers:<String,String>{
         'Content-Type': 'application/json; charset=UTF-8',
      },
      
    );
  }
  Future<http.Response> subtractcart(String item_id,String userid) async {
    return http.get(
      Uri.parse('http://192.168.0.163:4000/api/Login/SubtractCart?item_name=$item_id&userid=$userid'),
      headers:<String,String>{
         'Content-Type': 'application/json; charset=UTF-8',
      },
      
    );
  }
  Future<http.Response> order(OrderClass l) async {
    return http.post(
      Uri.parse('http://192.168.0.163:4000/api/Login/order'),
      headers:<String,String>{
         'Content-Type': 'application/json; charset=UTF-8',
      },
      body: json.encode(l),
    );
  }
 Future<http.Response> deleteallcart(String userid) async {
    return http.get(
      Uri.parse('http://192.168.0.163:4000/api/Login/DeleteAllCart?userid=$userid'),
      headers:<String,String>{
         'Content-Type': 'application/json; charset=UTF-8',
      },
      
    );
  }
   Future<http.Response> gethistory(String userid) async {
    return http.get(
      Uri.parse('http://192.168.0.163:4000/api/Login/Gethistory?userid=$userid'),
      headers:<String,String>{
         'Content-Type': 'application/json; charset=UTF-8',
      },
      
    );
  }
  Future<http.Response> getjuice() async {
    return http.get(
      Uri.parse('http://192.168.0.163:4000/api/Login/Addjuice'),
      headers:<String,String>{
         'Content-Type': 'application/json; charset=UTF-8',
      },
      
    );
  }
  Future<http.Response> getbarprice(String barid) async {
    return http.get(
      Uri.parse('http://192.168.0.163:4000/api/Login/Getbarprice?bid=$barid'),
      headers:<String,String>{
         'Content-Type': 'application/json; charset=UTF-8',
      },
      
    );
  }
  Future<http.Response> getfruitprice(String fid) async {
    return http.get(
      Uri.parse('http://192.168.0.163:4000/api/Login/Getfruitjuice?fid=$fid'),
      headers:<String,String>{
         'Content-Type': 'application/json; charset=UTF-8',
      },
      
    );
  }
    Future<http.Response> insertjuice(String name,int total,String userid) async {
    return http.get(
      Uri.parse('http://192.168.0.163:4000/api/Login/insertjuice?name=$name&total=$total&userid=$userid'),
      headers:<String,String>{
         'Content-Type': 'application/json; charset=UTF-8',
      },
      
    );
  }
   Future<http.Response> insertjuicehistory(JuiceClass jm) async {
    return http.post(
      Uri.parse('http://192.168.0.163:4000/api/Login/juicehistory'),
      headers:<String,String>{
         'Content-Type': 'application/json; charset=UTF-8',
      },
       body: json.encode(jm),
    );
  }
  Future<http.Response> getrjiuice() async {
    return http.get(
      Uri.parse('http://192.168.0.163:4000/api/Login/GetRecommended'),
      headers:<String,String>{
         'Content-Type': 'application/json; charset=UTF-8',
      },
       
    );
  }
  Future<http.Response> getingre(String jname1) async {
    return http.get(
      Uri.parse('http://192.168.0.163:4000/api/Login/GetIngredients?jname1=$jname1'),
      headers:<String,String>{
         'Content-Type': 'application/json; charset=UTF-8',
      },
       
    );
  }
  Future<http.Response> addrjuice(String jid,String userid) async {
    return http.get(
      Uri.parse('http://192.168.0.163:4000/api/Login/AddRecommended?jid=$jid&userid=$userid'),
      headers:<String,String>{
         'Content-Type': 'application/json; charset=UTF-8',
      },
       
    );
  }
  
  


}