import 'package:flutter/material.dart';
import 'package:fruitsui/fruits.dart';
import 'package:fruitsui/home1.dart';
import 'package:fruitsui/juice.dart';
import 'package:fruitsui/orderclass.dart';
import 'package:fruitsui/vegetable.dart';
import 'login.dart';
import 'package:fruitsui/api.dart';
import 'dart:convert';
import './home.dart';
import './myorder.dart';
import 'juice.dart';

import 'order.dart';


class CartPage extends StatefulWidget {
  @override
  CartPageState createState() =>
      new CartPageState();
}
class CartPageState extends State<CartPage> {
   final globalKey = GlobalKey<ScaffoldState>();
   final _fname = TextEditingController();
  final _lname = TextEditingController();
  final _email = TextEditingController();
  final _phone = TextEditingController();
  final _address = TextEditingController();
  final _city = TextEditingController();
  final _state = TextEditingController();
  final _zip = TextEditingController();
  int Total = 0;
  int Quantity =0;
bool log = true;
    List<dynamic> list;
  bool _autoValidate = false;
  bool cart=false;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  void initState() {
 getcart();
    // This is the initial data // Set it in initState because you are using a stateful widget
super.initState();
  }
  void getcart() async{
    API.Api.getcart(id.toString()).then((value) {
          var data = json.decode(value.body);
          var res = data as List;
          setState(() {
            list=res;
            for(int i=0;i<list.length;i++)
            {
              Total = Total + list[i]['Price'];
              Quantity = Quantity + list[i]['Quantity'];
            }
            cart=true;
          });
          });
  }
    Widget build(BuildContext context) {
    return Scaffold(
      key: globalKey,
      drawer: Drawer(child: ListView(
        padding: EdgeInsets.zero,
        children: [
           DrawerHeader(
        child: Text(''),
        decoration: BoxDecoration(
          color: Colors.white,
        ),
      ),
      ListTile(
              leading: Icon(Icons.home,color: Colors.green[800],),
        title: Text('Home'),
        
        onTap: () {
        if(id==0)
        {
           Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => MyHomePage()),
  ); }
        else
        { Navigator.push(context,
    MaterialPageRoute(builder: (context) => MyHomePage_l1()),
  ); } }
      ),
            ListTile(
              leading: Icon(Icons.star_border,color: Colors.green[800],),
        title: Text('Fruits'),
        
        onTap: () {
          Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => FruitsPage()),
  );
          // Update the state of the app.
          // ...
        },
      ),
       ListTile(
              leading: Icon(Icons.star_border,color: Colors.green[800],),
        title: Text('Vegetables'),
        
        onTap: () {
          Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => VegetablePage()),
  );
          // Update the state of the app.
          // ...
        },
      ),      ListTile(
              leading: Icon(Icons.star_border,color: Colors.green[800],),
        title: Text('Juices'),
        
        onTap: () {
          if (id !=0) {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => JuicesPage()));
        } else  {
         
           
            Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) =>LoginPage(login_page: 1)));
        }},
      ),
        ListTile(
              leading: Icon(Icons.history,color: Colors.green[800],),
        title: Text('My Orders'),
        onTap: () {
           Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => OrderPage()),
  );  }
      ), 

      log?ListTile(
              leading: Icon(Icons.logout,color: Colors.green[800],),
        title: Text('Logout'),
        
        onTap: () {
         logout(id.toString());

          }
       
      ):ListTile(
              leading: Icon(Icons.person,color: Colors.green[800],),
        title: Text('Login'),
        
        onTap: () {
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => LoginPage(login_page:3 ,)));

          }
       
      )
      ],
      ),),
       body:
       Column(
         children:[
           Expanded(child:   ListView( 
      children: <Widget>[
        SizedBox(
          height: 10.0,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.menu, color: Colors.grey),
              onPressed: () {
                globalKey.currentState.openDrawer();
              },
            ),
            Spacer(),
            Container(
              height: 50.0,
              width: 200.0,
              decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.1),
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(25.0),
                      bottomLeft: Radius.circular(25.0),
                      topLeft: Radius.circular(25.0),
                      topRight: Radius.circular(25.0))),
              child: TextField(
                decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(top: 15.0),
                    prefixIcon: Icon(Icons.search, color: Colors.grey)),
              ),
            ),
            Spacer(),
          IconButton(
                  icon: Icon(
                    Icons.logout,
                    color: Colors.green[800],
                  ),
                  onPressed: null,
                ),

           
          ],
        ),
        SizedBox(height: 10.0),
        Row(children: [
          Padding(
          padding: EdgeInsets.only(left: 10.0),
          child: Text('Cart',
              style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontWeight: FontWeight.bold,
                  fontSize: 25.0)),
        ),
        Spacer(),
       
        ],),
        
            
        
      Container(
height: 300,
  child: cart ? ListView.builder(
                    shrinkWrap: true,
                    itemCount: list.length,
                    
                    //scrollDirection: Axis.horizontal,
                    itemBuilder: (BuildContext context, int index) {
                      return new GestureDetector(
                        onTap: () {
                         
                        },
                        child: Container(
                          margin: const EdgeInsets.only(
                              top: 10.0, left: 5.0, right: 5.0, bottom: 10.0),
                     
                          decoration: BoxDecoration(
                            border: Border(bottom: BorderSide(width: 1.0,color: Colors.green[800])),
                          
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                             mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(left: 5),
                                    child: CircleAvatar(
                                        radius: 30,
                                        backgroundColor: Colors.transparent,
                                        child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(100),
                                            child: Image.asset(
                                             'assets/bg_1.jpg',
                                              width: 100,
                                              height: 100,
                                              fit: BoxFit.cover,
                                            ))),
                                  ),
                                  Row(
                                    children: [
                                      Column(
                                        children: [
                                          Padding(padding: EdgeInsets.only(left:5.0),
                                          child:   Text(
                                          list[index]["item_name"],
                                         
                                          style: TextStyle(
                                              color: Colors.black,
                                           
                                              fontSize: 18),
                                        ),
                                          ),
                                             Padding(padding: EdgeInsets.only(left:5.0),child:                                   Text(
                                          // "Name: " + lists[index]["cname"],
                                         "Price: ${list[index]['Price']}",
                                          style: TextStyle(
                                              color: Colors.grey,
                                            
                                              fontSize: 12),
                                        ),),
     
                                      
                                         Text(
                                          // "Name: " + lists[index]["cname"],
                                  " Total: ${list[index]['Total']}",
                                          style: TextStyle(
                                              color: Colors.grey,
                                            
                                              fontSize: 12),
                                        ),
                                           IconButton(
          icon: const Icon(Icons.delete,color: Color(0xFF2E7D32)),
          tooltip: 'Increase volume by 10',
          onPressed: () {
         deletecart(list[index]["item_name"], id.toString(), index);
          },
        ),
                                      
                                        ],

                                      )
                                    
                                    ],
                                  ),
                                  Spacer(),
                                    Row(
                                      children:[
              IconButton(
          icon: const Icon(Icons.add,color: Color(0xFF2E7D32)),
       
          onPressed: () {
            addcart(list[index]["item_name"], id.toString());
          },
        ),
       Container(
  height: 50,
  width: 50,
  decoration: BoxDecoration(
    border: Border.all(
      color: Colors.green[800],
    ),
    borderRadius: BorderRadius.circular(5.0),
  ),
  child: Center(
    child: Text("${list[index]['Quantity']}"),
  ),
),       
 IconButton(
          icon: const Icon(Icons.remove,color: Color(0xFF2E7D32)),
       
          onPressed: () {
           subtractcart(list[index]["item_name"], id.toString());
          },
        ),              
                                           ]     ),
                                 
                                ],
                              ),
                            ],
                          ),
                        ),
                      );
                    }):Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                ],
              ))
),
SizedBox(height:30.0),
Container(
  child:SingleChildScrollView(child: Form

        ( key: _formKey,
        
          
          child: Column(
          children:[
                        Container(
  height: 50,
 
  decoration: BoxDecoration(
    border: Border.all(
      color: Colors.green[800],
    ),
    borderRadius: BorderRadius.circular(5.0),
    color: Colors.green[800],
  ),
  child:Row(
    children:[
      Padding(padding: EdgeInsets.only(left:
      10.0,),
      child:Text('Total: Rs $Total',style: TextStyle(color:Colors.white,fontSize:20.0),),
      ),

Spacer(),
  Padding(padding: EdgeInsets.only(right:
      10.0,),
      child:Text('Quantity: $Quantity',style: TextStyle(color:Colors.white,fontSize:20.0),),
      ),
    ] 
  ),
  
),
SizedBox(
  height:10.0
),

TextFormField(
  enabled: false,
      controller: _fname,
     decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: name,
         labelStyle: TextStyle(color: Colors.green[800]),
        hintText: name,
      ),
      keyboardType: TextInputType.text,
     
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    ),
    SizedBox(
  height:10.0
),
TextFormField(
      controller: _phone,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "Phone",
        labelStyle: TextStyle(color: Colors.black),
        hintText: "",
      ),
      textInputAction: TextInputAction.done,
        validator: (String args) {
        if (args == "") {
          return 'Please fill this field';
        } else if (args.length < 11 || args.length > 11) {
          return 'Invalid Phone';
        } else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    ),

    SizedBox(
  height:10.0
),
TextFormField(
      controller: _address,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "Address",
        labelStyle: TextStyle(color: Colors.black),
        hintText: "",
      ),
      textInputAction: TextInputAction.done,
      validator: (String args) {
        if (args == "")
          return 'Please fill this field';
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    ),
 SizedBox(
  height:10.0
),
  TextFormField(
      
      controller: _email,
      //focusNode: _emailFocusNode,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: "Email",
          labelStyle: TextStyle(color: Colors.black),
          hintText: ""
          //hintText: "e.g abc@gmail.com",
          ),

      textInputAction: TextInputAction.next,

      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    ),
SizedBox(height:10.0),
TextFormField(
      controller: _city,
     decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "City",
        labelStyle: TextStyle(color: Colors.black),
        hintText: "",
      ),
      keyboardType: TextInputType.text,
      validator: (String arg) {
         if (arg == "") {
          return 'Please fill this field';
        } 
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    ),
    SizedBox(height:10.0),
TextFormField(
      controller: _state,
     decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "State",
        labelStyle: TextStyle(color: Colors.black),
        hintText: "",
      ),
      keyboardType: TextInputType.text,
      validator: (String arg) {
         if (arg == "") {
          return 'Please fill this field';
        } 
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    ),
        SizedBox(height:10.0),
TextFormField(
      controller: _zip,
     decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "Zip",
        labelStyle: TextStyle(color: Colors.black),
        hintText: "",
      ),
      keyboardType: TextInputType.text,
      validator: (String arg) {
        if (arg == "") {
          return 'Please fill this field';
        } 
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    ),
    
  RaisedButton(
                onPressed: _validateInputs,
                color: Colors.green[800],
                textColor: Colors.white,
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: Text(
                  'Place Order',
                  style: TextStyle(fontSize: 15),
                ),
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
              ),

          ]

        )),)
)]
    ),),]
       ));
    }
        void _validateInputs() {
    if (_formKey.currentState.validate()) {
          OrderClass order = new OrderClass.novalue();
          order.fname = name;
          order.lname = _lname.text;
          order.email = _email.text;
          order.phone = _phone.text;
          order.address = _address.text;
          order.city = _city.text;
          order.state = _state.text;
          order.zip = _zip.text;
          order.total = Total;
          order.user_id = id.toString();
          API.Api.order(order).then((value) {
            if(value.statusCode ==  200)
            {
              print("hello");
            }
          });
         Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => Order()));
      
      }
    else {
//    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }
  void deletecart(String item,String user,int index) async
  {
    API.Api.deletefromcart(item, user).then((value) {
      if(value.statusCode == 200)
      {
setState(() {
        list.remove(index);
         Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => CartPage()));
      });
   
      }
       });
    }
    void addcart(String item,String user) async
  {
    API.Api.addincart(item, user).then((value) {
      if(value.statusCode == 200)
      {
setState(() {
        
         Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => CartPage()));
      });
   
      }
       });
    }
        void subtractcart(String item,String user) async
  {
    API.Api.subtractcart(item, user).then((value) {
      if(value.statusCode == 200)
      {
setState(() {
        
         Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => CartPage()));
      });
   
      }
       });
    }
    logout(String uid)
  {
    API.Api.deleteallcart(uid).then((value) {
      if(value.statusCode == 200)
      {
        setState(() {
           id=0;
        name=null;
        });
       
           Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => MyHomePage(),
        ));
      }
    });
  }
}