class OrderClass{
  String orderno;
  String fname;
  String lname;
  String email;
  String phone;
  String address;
  String city;
  String state;
  String zip;
  int total;
  String user_id;
  String order_date;

factory OrderClass.fromJson(Map<String, dynamic> json){
  return OrderClass(
    json['Orderno'],
    json['fname'],
    json['lname'],
    json['email'],
    json['phone'],
    json['address'],
    json['city'],
    json['state'],
    json['Zip'],
    json['total'],
    json['user_id'],
    json['order_date'],
  );
}
OrderClass.novalue();
Map<String,dynamic> toJson()=>{
'Orderno': orderno,
'fname': fname,
'lname': lname,
'email': email,
'phone': phone,
'address': address,
'city': city,
'state': state,
'Zip': zip,
'total': total,
'user_id':user_id,
'order_date': order_date,
};
OrderClass(
  this.orderno,
  this.fname,
  this.lname,
  this.email,
  this.phone,
  this.address,
  this.city,
  this.state,
  this.zip,
  this.total,
  this.user_id,
  this.order_date
);
}