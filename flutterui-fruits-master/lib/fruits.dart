import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:io';
import 'package:fruitsui/api.dart';
import 'package:fruitsui/cart.dart';
import 'package:fruitsui/home.dart';
import 'package:badges/badges.dart';
import 'package:fruitsui/vegetable.dart';
import './myorder.dart';
import './juice.dart';
import './home1.dart';
import 'login.dart';
class FruitsPage extends StatefulWidget {
  @override
  _FruitsPageState createState() => _FruitsPageState();
}

class _FruitsPageState extends State<FruitsPage> {
  @override
     List<dynamic> list;
      final globalKey = GlobalKey<ScaffoldState>();
     bool fruit = false;
     bool cart= false;
     bool platform = false;
     bool log = false;
     var count;
     void initState() {
       if(Platform.isIOS)
       {
          platform = true;
       }
 getfruit();
 if(id!=0)
 {
   log= true;
    getcartcount();
 }

    // This is the initial data // Set it in initState because you are using a stateful widget
super.initState();
  }
    void getfruit() async{
    API.Api.fruit().then((value) {
      var data = json.decode(value.body);
      var res = data as List;
      setState(() {
        list = res;
        fruit = true;
      });
      
    });
  }
  void getcartcount() async {
    API.Api.getcartcount(id.toString()).then((value){
      
      setState(() {
        count = json.decode(value.body);
        cart=true;
      });
    });
    
  }
  String generate_path(String url)
  {
    
      String path = "/Users/apple/Desktop/development/flutterui-fruits-master"; 
    String result = url.replaceAll("~", path);
   print(result);
    return result;
    
  }
  Widget build(BuildContext context) {


  return Scaffold(
  
    backgroundColor: Colors.white,
     key: globalKey,
      appBar: AppBar(
          title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                 IconButton(
              icon: Icon(Icons.menu, color: Colors.green[800]),
              onPressed: () {
                globalKey.currentState.openDrawer();
              },
            ),
                Spacer(),
                Text(
                  "Fruits",style: TextStyle(color: Colors.green[800]),
                  textAlign: TextAlign.center,
                ),
                Spacer(),

              cart?    Badge(
      badgeContent: Text("$count",style: TextStyle(color:Colors.white),),
      child:  IconButton(
        icon: Icon(Icons.shopping_cart,color: Colors.green[800],),
      
        onPressed: (){
          
        if (name !=null) {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => CartPage()));
        } else  {
         
           
            Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) =>LoginPage(login_page: 2)));
        }
        },
      ),
      badgeColor: Colors.green[800],
      
    ):Spacer(),
                log?IconButton(
                  icon: Icon(
                    Icons.logout,
                    color: Colors.green[800],
                  ),
                  onPressed: () => logout(id.toString())
                ):
                IconButton(
                  icon: Icon(
                    Icons.person,
                    color: Colors.green[800],
                  ),
                  onPressed: () {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => LoginPage(login_page:3 ,)));
                  })

              ]),
          backgroundColor: Colors.white,
          automaticallyImplyLeading: false,
          elevation: 0.0,
        ),
      drawer: Drawer(child: ListView(
        padding: EdgeInsets.zero,
        children: [
           DrawerHeader(
        child: Text(''),
        decoration: BoxDecoration(
          color: Colors.white,
        ),
      ),
      ListTile(
              leading: Icon(Icons.home,color: Colors.green[800],),
        title: Text('Home'),
        
        onTap: () {
        if(id==0)
        {
           Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => MyHomePage()),
  ); }
        else
        { Navigator.push(context,
    MaterialPageRoute(builder: (context) => MyHomePage_l1()),
  ); } }
      ),
            ListTile(
              leading: Icon(Icons.star_border,color: Colors.green[800],),
        title: Text('Fruits'),
        
        onTap: () {
          Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => FruitsPage()),
  );
          // Update the state of the app.
          // ...
        },
      ),
       ListTile(
              leading: Icon(Icons.star_border,color: Colors.green[800],),
        title: Text('Vegetables'),
        
        onTap: () {
          Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => VegetablePage()),
  );
          // Update the state of the app.
          // ...
        },
      ),      ListTile(
              leading: Icon(Icons.star_border,color: Colors.green[800],),
        title: Text('Juices'),
        
        onTap: () {
          if (id !=0) {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => JuicesPage()));
        } else  {
         
           
            Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) =>LoginPage(login_page: 1)));
        }},
      ),
        ListTile(
              leading: Icon(Icons.history,color: Colors.green[800],),
        title: Text('My Orders'),
        onTap: () {
           Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => OrderPage()),
  );  }
      ), 

      log?ListTile(
              leading: Icon(Icons.logout,color: Colors.green[800],),
        title: Text('Logout'),
        
        onTap: () {
         logout(id.toString());

          }
       
      ):ListTile(
              leading: Icon(Icons.person,color: Colors.green[800],),
        title: Text('Login'),
        
        onTap: () {
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => LoginPage(login_page:3 ,)));

          }
       
      )
      ],
      ),),
    body:Container(
      child: fruit ? new ListView.builder(
        shrinkWrap: true,
        itemCount: list.length,
        itemBuilder: (BuildContext context , int index){
          return new GestureDetector(
            onTap: (){
              if(id== 0)
              {
            
          Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => LoginPage(login_page: 0,itemid:list[index]['id'])),
  );
              }
  else
  {
        API.Api.cart(list[index]['id'], id.toString()).then((value) {
        if(value.statusCode == 200)
        {
          showAlertDialog(context);  
        }
        });
              }
            },
            child: Container(
               margin: const EdgeInsets.only(
                              top: 10.0, left: 5.0, right: 5.0, bottom: 10.0),
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            border: Border(bottom: BorderSide(width: 1.0,color: Colors.grey)),
                          
                          ),
                                child: Column(
                            
                            // mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Row(
                              
                                children: <Widget>[
                                   SizedBox(
          height: 10.0,
        ),
         
                                  platform?Padding(
                                    padding: EdgeInsets.only(left: 5),
                                    child: CircleAvatar(
                                        radius: 40,
                                        backgroundColor: Colors.transparent,
                                        child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(100),
                                            child:Image(image:AssetImage('assets/star.png'),
                                              width: 80,
                                              height: 80,
                                              fit: BoxFit.cover,
                                            ))),
                                  ):
                                  Padding(
                                    padding: EdgeInsets.only(left: 5),
                                    child: CircleAvatar(
                                        radius: 40,
                                        backgroundColor: Colors.transparent,
                                        child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(100),
                                            child: Image(image:AssetImage('assets/star.png'),
                                              width: 100,
                                              height: 100,
                                              fit: BoxFit.cover,
                                            ))),
                                  ),
                                  Row(
                                    children:[
  Padding(padding: EdgeInsets.only(left:20),
                                child: Text(list[index]['name'],style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),)),
                               
                                 Padding(padding: EdgeInsets.only(left:50),
                                child: Text("Price: ${list[index]['price']}",style: TextStyle(fontSize: 16,color: Colors.green[800]))),
                                    ]
                                  )
                              
           
                                ],
                              ),
                            ],
                          ),
            ),
          );
        },
      ):Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                ],
              ))
    ) ,
  );
      
    
  }
  showAlertDialog(BuildContext context) {  
  // Create button  
  Widget Shop = FlatButton(  
    child: Text("Continue Shopping"),  
    onPressed: () {  
      Navigator.of(context).pop();  
    },  
   
  );  
  Widget Checkout = FlatButton(  
    child: Text("Checkout"),  
   onPressed: () {  
       Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => CartPage()),
  );
    },  
  );  
  
  // Create AlertDialog  
  AlertDialog alert = AlertDialog(  
    title: Text("Wohoo",style: TextStyle(color:Colors.green[800]),),  
    content: Text("Item Added to Cart"),  
    actions: [  
      Shop,
      Checkout  
    ],  
  );  
  
  // show the dialog  
  showDialog(  
    context: context,  
    builder: (BuildContext context) {  
      return alert;  
    },  
  );  
} 
  logout(String uid)
  {
    API.Api.deleteallcart(uid).then((value) {
      if(value.statusCode == 200)
      {
        setState(() {
           id=0;
        name=null;
        });
       
           Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => MyHomePage(),
        ));
      }
    });
  }
  }
