import 'package:flutter/material.dart';
import 'cart.dart';
import 'login.dart';
import 'order.dart';
class Checkout extends StatefulWidget{
  Checkout_State createState() => new Checkout_State();
}
class Checkout_State extends State<Checkout>
{
  final _username = TextEditingController();
  final _email = TextEditingController();
  final _phone = TextEditingController();
  final _address = TextEditingController();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
 bool _autoValidate = false;
   Widget build(BuildContext context) {
         print(name);
    return Scaffold(
    
        appBar: AppBar(
          title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.green[800],
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                Spacer(),
                Text(
                  "Checkout",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color:Colors.green[800],
                  ),
                ),
                Spacer(),
                IconButton(
                  icon: Icon(
                    Icons.logout,
                    color: Colors.white,
                  ),
                onPressed: (){},
                ),
              ]),
          backgroundColor: Colors.white,
          automaticallyImplyLeading: false,
          elevation: 5.0,
        ),
        body: SingleChildScrollView(child: Form

        ( key: _formKey,
        
          
          child: Column(
          children:[
                        Container(
  height: 50,
 
  decoration: BoxDecoration(
    border: Border.all(
      color: Colors.green[800],
    ),
    borderRadius: BorderRadius.circular(5.0),
    color: Colors.green[800],
  ),
  child:Row(
    children:[
      Padding(padding: EdgeInsets.only(left:
      10.0,),
      child:Text('Total Rs:200',style: TextStyle(color:Colors.white,fontSize:20.0),),
      ),

Spacer(),
  Padding(padding: EdgeInsets.only(right:
      10.0,),
      child:Text('Quantity: 2',style: TextStyle(color:Colors.white,fontSize:20.0),),
      ),
    ] 
  ),
  
),
SizedBox(
  height:10.0
),

TextFormField(
      controller: _username,
     decoration: InputDecoration(
          labelText: " Name",
          fillColor: Colors.white,
          focusedBorder:OutlineInputBorder(
            borderSide: const BorderSide(color: Color(0xFF2E7D32), width: 2.0),
            borderRadius: BorderRadius.circular(5.0),
          ),
        ),
      keyboardType: TextInputType.text,
      validator: (String arg) {
        if (arg.length < 3)
          return 'Name must be more than 2 charater';
        else if (arg == "") {
          return 'Please fill this field';
        } 
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    ),
    SizedBox(
  height:10.0
),

TextFormField(
     controller: _phone,
      keyboardType: TextInputType.number,
     decoration: InputDecoration(
          labelText: " Phone",
          fillColor: Colors.white,
          focusedBorder:OutlineInputBorder(
            borderSide: const BorderSide(color: Color(0xFF2E7D32), width: 2.0),
            borderRadius: BorderRadius.circular(5.0),
          ),
        ),
      textInputAction: TextInputAction.done,
      validator: (String args) {
        if (args == "") {
          return 'Please fill this field';
        } else if (args.length < 11 || args.length > 11) {
          return 'Invalid Phone';
        } else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    ),
    SizedBox(
  height:10.0
),
TextFormField(
      controller: _address,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "Address",
        labelStyle: TextStyle(color: Colors.black),
        hintText: "",
      ),
      textInputAction: TextInputAction.done,
      validator: (String args) {
        if (args == "")
          return 'Please fill this field';
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    ),
 SizedBox(
  height:10.0
),
  TextFormField(
      enabled: false,
      controller: _email,
      //focusNode: _emailFocusNode,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: name,
          labelStyle: TextStyle(color: Colors.green[800]),
          hintText: name
          //hintText: "e.g abc@gmail.com",
          ),

      textInputAction: TextInputAction.next,

      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    ),

  RaisedButton(
                onPressed: _validateInputs,
                color: Colors.green[800],
                textColor: Colors.white,
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: Text(
                  'Place Order',
                  style: TextStyle(fontSize: 15),
                ),
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
              ),

          ]

        )),)
 
       
    );
   }
    void _validateInputs() {
    if (_formKey.currentState.validate()) {
     
         Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => Order()));
      
      }
    else {
//    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }

}