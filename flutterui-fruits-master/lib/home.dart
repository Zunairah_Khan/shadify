import 'package:flutter/material.dart';
import 'package:fruitsui/fruits.dart';
import 'package:fruitsui/juice.dart';
import 'package:fruitsui/vegetable.dart';


import 'login.dart';

class MyHomePage extends StatefulWidget {

  final String title;
  MyHomePage({Key key, this.title,})
      : super(key: key);

  @override
  _MyHomePageState createState() =>
      new _MyHomePageState();
}
class _MyHomePageState extends State<MyHomePage> {
  
  _MyHomePageState();
 
 
 Widget _buildTile(Widget child, {Function() onTap}) {
    return Material(
      elevation: 14.0,
      borderRadius: BorderRadius.circular(12.0),
      shadowColor: Color(0x802196F3),
      child: InkWell
      (
        // Do onTap() if it isn't null, otherwise do print()
        onTap: onTap != null ? () => onTap() : () { print('Not set yet'); },
        child: child
      )
    );
  }
   

  @override
  Widget build(BuildContext context) {
    if(name != null)
    {
        print(name);
    }
  
   //Future.delayed(Duration.zero, () => showAlert(context));
  return Scaffold(
      appBar: AppBar(
        title: Row(children: <Widget>[
           Text(
                  "Fridge It - Let's Buy Fresh",
                  textAlign: TextAlign.center,
                ),
                Spacer(),
                
                
           IconButton(
                  icon: Icon(
                    Icons.person,
                    color: Colors.white,
                  ),
                  onPressed: () { Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) =>LoginPage(login_page: 0)));},
                ),

        ]),
        backgroundColor: Colors.green[800],
        automaticallyImplyLeading: false,
        elevation: 5.0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
           
            Container(
                margin: const EdgeInsets.only(
                    top: 0.0, left: 5.0, right: 5.0, bottom: 50.0),
                padding: EdgeInsets.all(150),
                decoration: new BoxDecoration(
                    image: new DecorationImage(
                  image: new AssetImage("assets/bg_1.jpg"),
                  fit: BoxFit.cover,
                ))),
            Row(
              
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Spacer(),
                 _buildTile(
            Padding(
              padding: const EdgeInsets.all(38.0),
              child: Column
              (
                mainAxisAlignment: MainAxisAlignment.start,
                
                children: <Widget>
                [
                  Material
                  (
                    color: Colors.white,
                    shape: CircleBorder(),
                    child: Padding
                    (
                      padding: const EdgeInsets.only(right:20.0),

                      child: new Image.asset(
          'assets/watermelon_home.png'),
                    )
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 16.0)),
                  Text('Fruits     ', style: TextStyle(color: Colors.green[800], fontWeight: FontWeight.w700, fontSize: 25.0)),
                  
                ]
              ),
              
            ),
            onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => FruitsPage(),
          ),
          ),
          ),           
          Spacer(),
          _buildTile(
            Padding(
              padding: const EdgeInsets.all(38.0),
              child: Column
              (
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>
                [
                  Material
                  (
                    color: Colors.white,
                    shape: CircleBorder(),
                    child: Padding
                    (
                      padding: const EdgeInsets.all(5.0),
                      child: new Image.asset(
          'assets/carrot_home.png'),
                    )
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 16.0)),
                  Text('Vegetables', style: TextStyle(color: Colors.green[800], fontWeight: FontWeight.w700, fontSize: 20.0)),
                  
                ]
              ),
              
            ),
            onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => VegetablePage(),
          ),
          ),
          ),
          Spacer(),
         
              ],
            ),
        SizedBox(height:10.0),
                        Row(
              
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Spacer(),
                 _buildTile(
            Padding(
              padding: const EdgeInsets.all(38.0),
              child: Column
              (
                mainAxisAlignment: MainAxisAlignment.start,
                
                children: <Widget>
                [
                  Material
                  (
                    color: Colors.white,
                    shape: CircleBorder(),
                    child: Padding
                    (
                      padding: const EdgeInsets.only(right:20.0),

                      child: new Image.asset(
          'assets/orange-juice_home.png'),
                    )
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 16.0)),
                  Text('Juices    ', style: TextStyle(color: Colors.green[800], fontWeight: FontWeight.w700, fontSize: 25.0)),
                  
                ]
              ),
              
            ),
onTap: () {
        if (name !=null) {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => JuicesPage()));
        } else  {
         
           
            Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) =>LoginPage(login_page: 1)));
        }
      },     
            ),    
          Spacer(),
     
         
              ],
            ),
          ],
        ),
      ),
    );
  }
}
