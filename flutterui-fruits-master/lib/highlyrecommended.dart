import 'package:flutter/material.dart';
import 'package:fruitsui/api.dart';
import 'package:fruitsui/home1.dart';
import 'dart:convert';
import 'login.dart';
import './cart.dart';
class HighlyR extends StatefulWidget {
  
  @override
  _HighlyRState createState() => _HighlyRState();
}

class _HighlyRState extends State<HighlyR> {
  @override
     List<dynamic> list;
     List<String> lnames = List<String>();
     String test,test1;
     int index=0;
  bool print1,print2=false;
     List<String> ingredients = List<String>();
    Map<String,dynamic> order=new Map<String,dynamic>();
     bool fruit = false;
       void initState() {
 getorders();
super.initState();
       }
        void getorders() async{
    API.Api.getrjiuice().then((value) {
      var data = json.decode(value.body);
      var res = data as List;
      setState(() {
        list = res;
        fruit = true;
      });
      
    });

  }
  void addjuice(String fname) async{
   
    API.Api.getingre(fname).then((value) {
     var data = json.decode(value.body);
     var res = data as List;
     ingredients.clear();
      for(int i=0;i<res.length;i++)
      {
        
        ingredients.add(res[i]['ingredient']);
      
      }
     
    });
  }
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green[800],
      appBar: AppBar(
      elevation: 0.1,
      backgroundColor: Colors.green[800],
      title: Text("Highly Recommended"),
      
    ),
      body: Container(
      child:fruit? ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: list.length,
        itemBuilder: (BuildContext context,index) {
         addjuice(list[index]['jname']);
          return Card(
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: Container(
        decoration: BoxDecoration(color: Colors.white),
        child: ListTile(
        contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
        leading: Container(
          padding: EdgeInsets.only(right: 12.0),
          decoration: new BoxDecoration(
              border: new Border(
                  right: new BorderSide(width: 1.0, color: Colors.white24))),
          child: Icon(Icons.favorite, color: Colors.green),
        ),
        title: Text(
          list[index]['jname'],
          style: TextStyle(color: Colors.green[800], fontWeight: FontWeight.bold),
        ),
        //subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

        subtitle: print2?Column(
          children:[
            Row(children: 
            List.generate(ingredients.length, (index) {
              return Text("${ingredients[index]}"+" ");
            }),
                   )
          ]):SizedBox(height: 0.0,),
        trailing:
            Text("Rs. ${list[index]['price']}",style: TextStyle(color: Colors.green[800])),
    onTap: ()=> showAlertDialog(context,list[index]['jname']),
             
      
      )));
  
        } 
      ):Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                ],
              ))

    ),
     
    );
  }
  void addtocart(String jid)
  {
    API.Api.addrjuice(jid, id.toString()).then((value) {
      if(value.statusCode == 200)
      {
        print(value.body);
        showAlertDialog1(context);
      }
    });
  }
  showAlertDialog(BuildContext context,String jid) {  
  // Create button  
  Widget Shop = FlatButton(  
    child: Text("Cancel"),  
    onPressed: () {  
      Navigator.of(context).pop();  
    },  
   
  );  
  Widget Checkout = FlatButton(  
    child: Text("Confirm"),  
   onPressed: () {  
      addtocart(jid);

    },  
  );  
  
  // Create AlertDialog  
  AlertDialog alert = AlertDialog(  
    title: Text("Confirmation",style: TextStyle(color:Colors.green[800]),),  
    content: Text("Are you sure to add item to Cart?"),  
    actions: [  
      Shop,
      Checkout  
    ],  
  );  
  
  // show the dialog  
  showDialog(  
    context: context,  
    builder: (BuildContext context) {  
      return alert;  
    },  
  );  
}
showAlertDialog1(BuildContext context) {  
  // Create button  
  Widget Shop = FlatButton(  
    child: Text("Continue Shopping"),  
    onPressed: () {  
      Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => MyHomePage_l1()),
  );
    },  
   
  );  
  Widget Checkout = FlatButton(  
    child: Text("Checkout"),  
   onPressed: () {  
       Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => CartPage()),
  );
    },  
  );  
  
  // Create AlertDialog  
  AlertDialog alert = AlertDialog(  
    title: Text("Wohoo",style: TextStyle(color:Colors.green[800]),),  
    content: Text("Item Added to Cart"),  
    actions: [  
      Shop,
      Checkout  
    ],  
  );  
  
  // show the dialog  
  showDialog(  
    context: context,  
    builder: (BuildContext context) {  
      return alert;  
    },  
  );  
}  
}